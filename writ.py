from aiofiles import open as iopen
import asyncio
from pprint import pprint
from time import localtime
from csv import reader

def tempo_agr():
    dia = list(localtime())[2]
    mes = list(localtime())[1]
    ano = list(localtime())[0]
    return [dia, mes, ano]

dia, mes, ano = tempo_agr()[0], tempo_agr()[1], tempo_agr()[2]

async def escrever_first():
    async with iopen(f"csv/vendas.csv", "w") as f:
        await f.write("Dia;Vendas")

async def escrever():
    async with iopen(f"csv/vendas.csv", "a") as f:
        await f.write(f"\n{dia}/{mes}/{ano};127.01")

def ler():
    with open("csv/vendas.csv","r") as f:
        spam = reader(f)
        for x in spam:
            for y in x:
                if str(dia)+"/"+str(mes)+"/"+str(ano) == y:
                    print(f"achei -- {y}")
                pprint(y)
            pprint(x)
#loop = asyncio.get_event_loop()
#loop.run_until_complete(escrever())
