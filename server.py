from flask import Flask, render_template, request
import time

app = Flask(__name__)

@app.route('/', methods=['GET','POST'])
def index():
    tmp_now = list(time.localtime())
    if request.method == "POST":
        din_day = request.form.get("din")
        obs_day = request.form.get("obs")
        import pprint
        pprint.pprint("faturado hoje: "+ din_day + " Observação: " +obs_day)
        return render_template("index.html", temp=tmp_now)
    if request.method == "GET":
        return render_template("index.html", temp=tmp_now)

app.run(debug=True)
